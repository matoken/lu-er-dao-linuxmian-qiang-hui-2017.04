<!-- $theme: default -->

<!-- page_number: true -->
<!-- $size: 4:3 -->
<!-- footer: @matoken -->


# ext4のファイルベース暗号化FSを試してみる

##### KenichiroMATOHARA([@matoken](https://twitter.com/matoken/))
##### http://matoken.org 
### <s>[鹿児島Linux勉強会2017.04(2017-04-08)](https://kagolug.connpass.com/event/52477/)</s>
### <s>サンエールかごしま 小研修室3</s>
### ※時間切れ未発表

---

# [KenichiroMATOHARA](http://matoken.org)( [@matoken](https://twitter.com/matoken) )
  * 大隅から来ました
  * PC-UNIX/OSS, OpenStreetMap, 電子工作, 自転車……
  * altanativeが好き

![](./osmo.jpg)

---

# ext4のファイルベース暗号化FSを試してみる

---

# 昔やった似たような?発表

* [偏執的な人の為の? NotePCセキュリティ HDD編 -小江戸らぐ1月のオフな集まり(第102回)-](https://www.slideshare.net/matoken/notepc-hdd-1102 "偏執的な人の為の? NotePCセキュリティ HDD編 -小江戸らぐ1月のオフな集まり(第102回)-")

---

# よく使われてそうな暗号化ファイルシステム

* Windows : BitLocker
* MacOS : FileVault2
* Linux : dm-crypt(LUKS) / eCryptFS
* ValaCrypt(旧TrueCryptフォーク)

---

# なんでするの?

* メディアやPCを紛失しても安心
* ディスク廃棄時にそのまま廃棄できる
  * `shred`とか掛けるとすごく時間がかかってしまう
  * SSDなどの場合単純な上書きではデータが消去できない可能性がある(secure eraseは全体削除で使い勝手が良くない)

---

# 圧縮ファイルやgpgじゃ駄目?

* 暗号化FSは透過的に利用できる
認証を済ませるといつものように利用できる
(ログイン状態で放置するとデータ盗めるので注意)

---

# 重くなる?

* C2D以降やGeodeLXなどにはAES支援機能が入っているので軽い
```
$ grep -m 1 -o aes /proc/cpuinfo 
aes
```
* HDDでも1重なら体感速度は変わらない
* SSDでは2重(dm-crypt + eCryptFS)でも行ける感じ


---

# Linuxでの暗号化FSの種類

* ディスク全体
  * dm-crypt(LUKS)
    システム全体向け
  * ValaCrypt(マルチプラットホーム)
    持ち運び媒体など
* ファイル単位
  * eCryptFS
    Ubuntu等のHOMEの暗号化
  * EncFS
    オンラインストレージ等
  * ext4の暗号化機能
    Android向け
    
---

<!-- 
loop-AES, lessFS, 

---
-->

# ext4の暗号化機能

* Android/ChromeOS向けに開発(eCryptfsの初期開発者等)
* Linux 4.1で導入(メモリリークやファイルシステム破損のバグ修正が4.4で入っている)
* 暗号化アルゴリズム
  * AES-256-XTS, AES-256-CBC + CTS(ファイル名)
* ファイルベースの暗号化
  パーティション単位の暗号化は出来ない，指定ディレクトリ以下が暗号化される
  タイムスタンプ，オーナー，グループ，パーミッションなどは確認できてしまう
  パーミッションが許せば削除等も出来る

---

# 実際に使ってみる

---

# 条件

* Linux 4.1以上

```shell
$ uname -r
4.9.0-2-amd64
```

* CONFIG_EXT4_ENCRYPTIONが有効

```shell
$ grep CONFIG_EXT4_ENCRYPTION /boot/config-`uname -r`
CONFIG_EXT4_ENCRYPTION=y
```

- e2fsprogs 1.43以上

```shell
$ dpkg-query -W e2fsprogs
e2fsprogs       1.43.4-2
```

* ブロックサイズが4k

```
$ sudo dumpe2fs /dev/loop0 | grep -i 'block size'
dumpe2fs 1.43.4 (31-Jan-2017)
Block size:               4096
```

---

# 必要なパッケージの導入

```
$ sudo apt install e2fsprogs keyutils \
  util-linux coreutils mount
```

---

# ファイルシステムの用意

今回は既存のファイルシステム内にディスクイメージを作成してそれを利用

* 1GBのディスクイメージの作成

```shell
$ dd if=/dev/zero of=ext4-crypt.img seek=1073741824 bs=1 count=1
1+0 レコード入力
1+0 レコード出力
1 byte copied, 0.000118528 s, 8.4 kB/s
```

---

# パーティションの作成

primaryパーティションを1つ作成

```shell
$ /sbin/fdisk ext4-crypt.img
 :
Command (m for help): n
Partition type
   p   primary (0 primary, 0 extended, 4 free)
   e   extended (container for logical partitions)
Select (default p): p
Partition number (1-4, default 1): 
First sector (2048-2097151, default 2048): 
Last sector, +sectors or +size{K,M,G,T,P} (2048-2097151, default 2097151): 

Created a new partition 1 of type 'Linux' and of size 1023 MiB.

Command (m for help): w
The partition table has been altered.
Syncing disks.
```

---

# ext4でフォーマット

```shell
$ /sbin/mkfs.ext4 ./ext4-crypt.img 
mke2fs 1.43.4 (31-Jan-2017)
Found a dos partition table in ./ext4-crypt.img
Proceed anyway? (y,N) y
Discarding device blocks: done                            
Creating filesystem with 262144 4k blocks and 65536 inodes
Filesystem UUID: dc44fd43-7d7a-4dfc-87f1-dc52410e2dd1
Superblock backups stored on blocks: 
        32768, 98304, 163840, 229376

Allocating group tables: done                            
Writing inode tables: done                            
Creating journal (8192 blocks): done
Writing superblocks and filesystem accounting information: done
```

---

# マウント

```shell
$ sudo mount -o loop ./ext4-crypt.img /mnt
$ grep /mnt /etc/mtab 
/dev/loop0 /mnt ext4 rw,relatime,data=ordered 0 0
```

# オーナー，グループの変更

```shell
$ sudo chown `id -u`.`id -g` /mnt
$ ls -la /mnt
合計 36
drwxr-xr-x 3 mk   mk    4096  4月  2 04:58 .
drwxr-xr-x 1 root root   248  3月 28 02:19 ..
drwx------ 2 root root 16384  4月  2 04:58 lost+found
```

---

# ext4暗号化ファイルシステムの利用

---

# ext4の暗号化フラグを設定

```shell
$ sudo tune2fs -O encrypt /dev/loop0
$ sudo dumpe2fs /dev/loop0 | grep -io encrypt
dumpe2fs 1.43.4 (31-Jan-2017)
encrypt
```

---

# 鍵の生成とキーリングへの追加

```shell
$ /usr/sbin/e4crypt add_key
Enter passphrase (echo disabled): 
Added key with descriptor [07a3ce5a6ebf0396]
$ keyctl show
Session Keyring
1048296028 --alswrv   1000  1000  keyring: _ses
 615559430 --alsw-v   1000  1000   \_ logon: ext4:07a3ce5a6ebf0396
```

※パスフレーズの入力は1回だけで確認されないので初回は特に注意．利用しはじめる前にキーリングをクリアして登録し直してパスフレーズが正しいか確認しておく．

---

# 暗号化ポリシーの設定

このとき対象ディレクトリが空ではない場合エラーとなる( _`Error [Directory not empty] setting policy.`_ )ので注意．

マウントポイントには`lost+found`が存在するので必ずサブディレクトリ以下である必要がある．

```shell
$ mkdir /mnt/encryption
$ /usr/sbin/e4crypt set_policy 07a3ce5a6ebf0396 /mnt/encryption
Key with descriptor [07a3ce5a6ebf0396] applied to /mnt/encryption.
```

※鍵の生成とキーリングへの追加と暗号化ポリシーの設定は次のようにすることで一度に設定可能

```shell
$ /usr/sbin/e4crypt add_key /mnt/encryption
```

---

# 暗号化ファイルシステム領域にファイルを作成

```shell
$ echo 'hello' > /mnt/encryption/test.txt
$ ls -la /mnt/encryption
合計 12
drwxr-xr-x 2 mk mk 4096  4月  2 05:07 .
drwxr-xr-x 4 mk mk 4096  4月  2 05:06 ..
-rw-r--r-- 1 mk mk    6  4月  2 05:07 test.txt
```

---

# キーリングのクリア

```shell
$ sudo keyctl clear @s
$ sudo keyctl show
Session Keyring
1048296028 --alswrv   1000  1000  keyring: _ses
```

---

# キーリングをクリアしただけではアクセスできる

```shell
$ ls -lA /mnt/encryption
合計 12
-rw-r--r-- 1 mk mk    6  4月  2 05:07 test.txt
```

---

# アンマウントとマウントし直し

キーリングをクリアした状態でアンマウントすると暗号化された状態に戻る

```shell
$ sudo umount /mnt
$ sudo mount -o loop ./ext4-crypt.img /mnt
$ ls -la /mnt/encryption
合計 12
drwxr-xr-x 2 mk mk 4096  4月  2 05:42 .
drwxr-xr-x 4 mk mk 4096  4月  2 05:06 ..
-rw-r--r-- 1 mk mk    6  4月  2 05:42 uzUlJZQfaxMx,7cC63,53A
$ cat /mnt/encryption/uzUlJZQfaxMx,7cC63,53A 
cat: /mnt/encryption/uzUlJZQfaxMx,7cC63,53A: 要求されたキーが利用できません
```

ユーザ，グループ，パーミッションなどは見える．内容にはアクセスできない．

---

# 再度暗号化領域を利用出来るようにする

鍵の生成とキーリングへの追加と暗号化ポリシーの設定をし直すとアクセスできるようになる

```shell
$ /usr/sbin/e4crypt add_key /mnt/encryption
Enter passphrase (echo disabled): 
Added key with descriptor [07a3ce5a6ebf0396]
$ ls -la /mnt/encryption
合計 12
drwxr-xr-x 2 mk mk 4096  4月  2 05:42 .
drwxr-xr-x 4 mk mk 4096  4月  2 05:06 ..
-rw-r--r-- 1 mk mk    6  4月  2 05:42 test.txt
```

---

# ファイル名長の確認

eCryptFSなどはファイル名のメタデータがファイル名内にあるので利用できるファイル名長が短くなってしまう．ext4ではどうか試す．

* 通常のext4領域では256文字

```shell
$ touch /mnt/1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456
touch: '/mnt/1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456' に touch できません: ファイル名が長すぎます
$ touch /mnt/123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345
```
---

# 暗号化領域も同様だった

```shell
$ touch /mnt/encryption/123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345
$ ls -lA /mnt/encryption/
合計 4
-rw-r--r-- 1 mk mk 0  4月  2 07:14 123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345
-rw-r--r-- 1 mk mk 6  4月  2 05:42 test.txt
```

# 非暗号化状態ではこんな状態

```shell
-rw-r--r-- 1 mk mk    0  4月  2 07:14 _OsoePJvc3qPQCPHbUMtjSynszcHig3BL
-rw-r--r-- 1 mk mk    6  4月  2 05:42 uzUlJZQfaxMx,7cC63,53A
```

復号状態のファイル名は別の場所に記録されているよう．

---

# 複数の暗号化領域を作ってみる

* 新しい暗号化領域のためのディレクトリを作成

```shell
$ mkdir /mnt/encryption2
$ ls -la /mnt/encryption2
合計 8
drwxr-xr-x 2 mk mk 4096  4月  2 06:49 .
drwxr-xr-x 5 mk mk 4096  4月  2 06:49 ..
```

* 暗号化設定

```shell
$ sudo e4crypt add_key /mnt/encryption2
Enter passphrase (echo disabled):
Key with descriptor [9640dd016062b432] already exists
Key with descriptor [9640dd016062b432] applied to /mnt/encryption2.
$ keyctl show
Session Keyring   
1048296028 --alswrv   1000  1000  keyring: _ses
  94779002 --alsw-v      0     0   \_ logon: ext4:69ca01e214957173
 219437542 --alsw-v      0     0   \_ logon: ext4:07a3ce5a6ebf0396
1025344233 --alsw-v      0     0   \_ logon: ext4:9640dd016062b432
$ touch /mnt/encryption2/hoge
```

---

* 一回暗号化を解除してマウントし直す

```shell
$ keyctl clear @s
$ keyctl show
Session Keyring   
1048296028 --alswrv   1000  1000  keyring: _ses
$ sudo umount /mnt
$ sudo mount -o loop ./ext4-crypt.img /mnt
```

---

* 片方だけ鍵を登録して暗号化領域を利用

```shell
$ sudo e4crypt add_key /mnt/encryption2
Enter passphrase (echo disabled):
Added key with descriptor [9640dd016062b432]
Key with descriptor [9640dd016062b432] applied to /mnt/encryption2.
$ ls -la /mnt/encryption*
/mnt/encryption:  
合計 12
drwxr-xr-x 2 mk mk 4096  4月  2 06:11 .
drwxr-xr-x 5 mk mk 4096  4月  2 06:49 ..
-rw-r--r-- 1 mk mk    0  4月  2 06:11 _OsoePJvc3qPQCPHbUMtjSynszcHig3BL
-rw-r--r-- 1 mk mk    6  4月  2 05:42 uzUlJZQfaxMx,7cC63,53A

/mnt/encryption2: 
合計 8
drwxr-xr-x 2 mk mk 4096  4月  2 06:51 .
drwxr-xr-x 5 mk mk 4096  4月  2 06:49 ..
-rw-r--r-- 1 mk mk    0  4月  2 06:51 hoge
```

---

# 暗号化領域に鍵が登録されてない状態でファイルを作ってみる

暗号化領域に鍵が登録されてない状態でファイルを作るとどうなるかを確認．

```shell
$ ls -lA /mnt/encryption
合計 4
-rw-r--r-- 1 mk mk 0  4月  2 07:14 _OsoePJvc3qPQCPHbUMtjSynszcHig3BL
-rw-r--r-- 1 mk mk 6  4月  2 05:42 uzUlJZQfaxMx,7cC63,53A
mk@x220:~ (1180)$ touch /mnt/encryption/test
touch: '/mnt/encryption/test' のタイムスタンプを設定中です: そのようなファイルやディレクトリはありません
mk@x220:~ (1181)$ ls -lA /mnt/encryption
合計 4
-rw-r--r-- 1 mk mk 0  4月  2 07:14 _OsoePJvc3qPQCPHbUMtjSynszcHig3BL
-rw-r--r-- 1 mk mk 6  4月  2 05:42 uzUlJZQfaxMx,7cC63,53A
```

エラーとなって作れない．

---

## 別のユーザで利用

* 別のユーザで中が見えるか確認

```shell
$ id
uid=1001(gm) gid=1001(gm) groups=1001(gm),20(dialout),24(cdrom),25(floppy),29(audio),30(dip),44(video),46(plugdev),107(netdev)
$ ls -la /mnt/encryption
合計 12
drwxr-xr-x 2 mk mk 4096  4月  2 06:11 .
drwxr-xr-x 7 mk mk 4096  4月  2 07:48 ..
-rw-r--r-- 1 mk mk    0  4月  2 07:14 _OsoePJvc3qPQCPHbUMtjSynszcHig3BL
-rw-r--r-- 1 mk mk    6  4月  2 05:42 uzUlJZQfaxMx,7cC63,53A
$ ls -la /mnt/encryption
合計 12
drwxrwxrwx 2 mk mk 4096  4月  2 06:11 .
drwxr-xr-x 7 mk mk 4096  4月  2 07:48 ..
-rw-r--r-- 1 mk mk    0  4月  2 07:14 123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345
-rw-r--r-- 1 mk mk    6  4月  2 05:42 test.txt
```

---

* 権限があればファイル作成もできる

```shell
$ touch /mnt/encryption/other_user
$ ls -lA /mnt/encryption
合計 4
-rw-r--r-- 1 mk mk 0  4月  2 07:14 123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345
-rw-r--r-- 1 gm gm 0  4月  2 07:55 other_user
-rw-r--r-- 1 mk mk 6  4月  2 05:42 test.txt
```

---

* 暗号化解除は出来ないと思ったが，

```shell
$ /usr/sbin/e4crypt add_key /mnt/encryption
/mnt/encryption: Permission denied
```

* パーミッションをゆるくしてやると出来てしまう．

```shell
$ ls -la /mnt/encryption
合計 12
drwxrwxrwx 2 mk mk 4096  4月  2 07:55 .
drwxr-xr-x 7 mk mk 4096  4月  2 07:48 ..
-rw-r--r-- 1 gm gm    0  4月  2 07:55 97NmIBETx,1q9US96etRsA
-rw-r--r-- 1 mk mk    0  4月  2 07:14 _OsoePJvc3qPQCPHbUMtjSynszcHig3BL
-rw-r--r-- 1 mk mk    6  4月  2 05:42 uzUlJZQfaxMx,7cC63,53A
$ /usr/sbin/e4crypt add_key /mnt/encryption
Enter passphrase (echo disabled): 
Added key with descriptor [07a3ce5a6ebf0396]
Error [Permission denied] setting policy.
The key descriptor [07a3ce5a6ebf0396] may not match the existing encryption context for directory [/mnt/encryption].
$ ls -lA /mnt/encryption
合計 4
-rw-r--r-- 1 mk mk 0  4月  2 07:14 123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345
-rw-r--r-- 1 gm gm 0  4月  2 07:55 other_user
-rw-r--r-- 1 mk mk 6  4月  2 05:42 test.txt
```

---

# 使いどころ

* eCryptFSの代替として
  * eCryptFSの教訓を生かして実装しているので筋は良さそう
  * 速度も速くなるはず
  * ファイル名長もフルに使える
  * 透過的にログイン時自動マウントは自分で設定が必要
    多分eCryptFSの真似で行ける
* 未だあまり利用例がないのでバグとか心配
  * Androidで使われるようになったらきっと大丈夫?

---

# これから確認したいこと

* 暗号化seedを変更したり
* eCryptFSの代わりに`$HOME`を透過的に暗号化したり
  ：

---

# BtrFS……

* [FAQ - btrfs Wiki](https://btrfs.wiki.kernel.org/index.php/FAQ#Does_btrfs_support_encryption.3F "FAQ - btrfs Wiki")

> Btrfs does not support native file encryption (yet), and there's nobody actively working on it. It could conceivably be added in the future.

---

# 参考URL

* [本の虫: Ted Ts'oがEXT4で暗号化を実装](https://cpplover.blogspot.jp/2015/04/ted-tsoext4.html "本の虫: Ted Ts&apos;oがEXT4で暗号化を実装")
  [https://cpplover.blogspot.jp/2015/04/ted-tsoext4.html](https://cpplover.blogspot.jp/2015/04/ted-tsoext4.html)
* [Ext4 encryption [LWN.net]](https://lwn.net/Articles/639427/ "Ext4 encryption [LWN.net]")
  [https://lwn.net/Articles/639427/](https://lwn.net/Articles/639427/)
* ['[PATCH 00/22] ext4 encryption patches' - MARC](http://marc.info/?l=linux-ext4&m=142801360603286&w=2 "&apos;[PATCH 00/22] ext4 encryption patches&apos; - MARC")
  [http://marc.info/?l=linux-ext4&m=142801360603286&w=2](http://marc.info/?l=linux-ext4&m=142801360603286&w=2)