<!-- $theme: default>
<!-- page_number: true -->
<!-- $size: 4:3 -->
<!-- footer: @matoken -->


# PQI Air Pen Hack

##### KenichiroMATOHARA([@matoken](https://twitter.com/matoken/))
##### http://matoken.org 
### [鹿児島Linux勉強会2017.04(2017-04-08)](https://kagolug.connpass.com/event/52477/)
### サンエールかごしま 小研修室3

---

# [KenichiroMATOHARA](http://matoken.org)( [@matoken](https://twitter.com/matoken) )
  * PC-UNIX/OSS, OpenStreetMap, 電子工作, 自転車……
  * altanativeが好き，多様性は正義

![](./osmo.jpg)

---

# PQI Air Pen Hack



---

# PQI Air Pen

* 有線LANを無線LANにしてくれたり
* microSDをNAS的に使ったり
* バッテリ内蔵で1時間くらい単体動作可能
* 中はLinuxで出来ている
* ftp/telnetが空いていていじくれる
* NTT X-Storeで送料込み500円だった!
  * [「PQI Air Pen」Linux搭載でtelnetできるワイヤレスアクセスポイントが500円](http://hitoriblog.com/?p=48779)
  * 今はAmazonとかで1kくらい http://amzn.to/2ofxKxk

---

# 主な使い方

* 有線LANを繋いで電源を入れると無線LANのアクセスポイントになる
* microSDを接続しているとhttp/ftpでアクセスできるNASになる
* 電源Off状態でPCとUSB接続するとmicroSDリーダーになる

---

# 逸般的な使い方

* ftp/telnetで使う
* busyboxを差し替えて使いやすくする
* ボタンを利用してアプリケーションを制御
* LEDを利用して状況をお知らせ
* chrootでDebianのユーザランドを利用する
 :

---

# ちょっと叩いてみる

* dhcpの提供されているネットワークケーブルを繋いで電源をれてちょっと叩いてみる．
* まずはdhcp poolのipから探してみる．

```
$ sudo nmap -sP 192.168.2.200-
Starting Nmap 7.40 ( https://nmap.org ) at 2017-03-08 11:33 JST
  :
Nmap scan report for 192.168.2.214
Host is up (0.0012s latency).
MAC Address: 80:DB:31:01:A4:B8 (Power Quotient International)
  :
```

192.168.2.214でした．

---

# ポートスキャンしてみる

```
$ nmap -A 192.168.2.214

Starting Nmap 7.40 ( https://nmap.org ) at 2017-03-08 11:45 JST
Nmap scan report for 192.168.2.214
Host is up (0.037s latency).
Not shown: 995 closed ports
PORT     STATE SERVICE VERSION
21/tcp   open  ftp     vsftpd 2.0.7
23/tcp   open  telnet  BusyBox telnetd 1.0
53/tcp   open  domain  dnsmasq 2.52
| dns-nsid:
|_  bind.version: dnsmasq-2.52
80/tcp   open  http    Brivo EdgeReader access control http interface
|_http-title: PQI Air Pen
8080/tcp open  http    Mongoose httpd 3.7 (directory listing)
|_http-title: Index of /
Service Info: OS: Unix; Device: security-misc

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 37.14 seconds
```

---

```
$ nmap -P 0-65536 192.168.2.214

Starting Nmap 7.40 ( https://nmap.org ) at 2017-03-08 11:37 JST
Nmap scan report for 192.168.2.214
Host is up (0.035s latency).
Not shown: 995 closed ports
PORT     STATE SERVICE
21/tcp   open  ftp
23/tcp   open  telnet
53/tcp   open  domain
80/tcp   open  http
8080/tcp open  http-proxy

Nmap done: 1 IP address (1 host up) scanned in 0.61 seconds
```

---

# アクセスしてみる

* 80番ポートにアクセスすると認証無しで設定画面にアクセス出来る
* 8080番はファイルのアクセスが出来る．こちらも認証なし．

---

# ftp/telnetは要認証

```
$ nc 192.168.2.214 21
220 (vsFTPd 2.0.7)
USER anonimouse
331 Please specify the password.
PASS matoken@gmail.com
530 Login incorrect.
```

```
$ nc 192.168.2.214 23
������!����          (none) login: 

(none) login: 
```

---

# パケットキャプチャしてPQI Air PenのID/PASSを調べる

* ググれば判るけどせっかくなので調べる
* Android版のアプリがあってファイルのやり取りが出来る
* この通信内容を見るときっとアクセス方法が判る

---

# パケットキャプチャしてみる

適当なWi-Fiの使えるPCを用意してネットワークカードをmonitor modeにしてパケットキャプチャをします．
今回の環境はこんな感じ．

* PC : LENOVO Thinkpad X200
* NIC : Intel Corporation PRO/Wireless 5100 AGN
* OS : Ubuntu 17.04 amd64
* Driver : iwldvm, iwlwifi

---

### AQI Air Penの無線チャンネルを確認しておく

* ここでは11

```
$ nmcli d wifi | egrep 'SSID|PQI'
*  SSID                モード    CHAN  レート     信号  バー  セキュリティ 
   PQI Air Pen         インフラ  11    54 Mbit/s  100   ▂▄▆█  --           
```

```
$ sudo /sbin/iwlist wls1 scanning | grep -B 5 "PQI Air Pen"
		  Cell 09 - Address: 80:DB:31:01:A4:B7
					Channel:11
					Frequency:2.462 GHz (Channel 11)
					Quality=70/70  Signal level=-28 dBm  
					Encryption key:off
					ESSID:"PQI Air Pen"
```

---

### phyとネットワークインターフェイスの確認

```
$ /sbin/iw dev
phy#0
        Interface wls1
                ifindex 8
                wdev 0x3
                addr 00:22:fa:33:45:6a
                type managed
                channel 8 (2447 MHz), width: 20 MHz, center1: 2447 MHz
                txpower 15.00 dBm
```

---

### デバイスがmonitor modeになれるか確認する

```
$ /sbin/iw phy phy0 info | lv
  :
		Supported interface modes:
				 * IBSS
				 * managed
				 * monitor
  :
		software interface modes (can always be added):
				 * monitor
```

※monitorになれない場合はドライバを変更すると対応できる場合もあります．

---

### monitor modeのインターフェイスを作る

```
$ sudo iw phy phy0 interface add mon0 type monitor
```

### managed modeのインターフェイスを削除する

```
$ sudo iw dev wls1 del
```

### monitor modeのインターフェイス(mon0)をUpする

```
$ sudo ifconfig mon0 up
```

###  monitor modeのインターフェイスの無線チャンネルを設定する

上の方で11チャンネルだったので2462に設定します．

```
$ sudo iw dev mon0 set freq 2462
```

---

他のチャンネルはこんな感じ

>
ch1 : 2412  
ch2 : 2417  
ch3 : 2422  
ch4 : 2427  
ch5 : 2432  
ch6 : 2437  
ch7 : 2442  
ch8 : 2447  
ch9 : 2452  
ch10 : 2457  
ch11 : 2462  
ch12 : 2467  
ch13 : 2472  
ch14 : 2484  

---

### 確認

```
$ /sbin/iwconfig mon0
```

---

## パケットキャプチャをしながらスマートフォン公式アプリを使ってみる

tcpdumpでパケットをキャプチャしながらスマートフォンでPQi Air Penのネットワークに繋いだ状態で公式アプリを起動して更新などを行います．

※パケットがたくさん飛んでいるような場合はフィルタを書いたりWiresharkなどを使うと便利です．

---

```
$ sudo tcpdump -i mon0 -n -A -s0
    :
01:27:10.970158 1.0 Mb/s 2462 MHz 11b -34dBm signal antenna 3 IP 192.168.200.1.21 > 192.168.200.102.50504: Flags [P.], seq 1:21, ack 0, win 2896, options [nop,nop,TS val 194874 ecr 35416851], length 20: FTP: 220 (vsFTPd 2.0.7)
E..Hv.@.@..-.......f...H.[.....;...P.P.....
...:..k.220 (vsFTPd 2.0.7)
...e
    :
```

* FTP

---

```
01:26:05.791087 2462 MHz 11n -39dBm signal antenna 3 72.2 Mb/s MCS 7 20 MHz s
hort GI mixed IP 192.168.200.102.50396 > 192.168.200.1.21: Flags [P.], seq 1:
12, ack 20, win 115, options [nop,nop,TS val 35410347 ecr 178581], length 11:
 FTP: USER root
E..?O.@.@..z...f.............wu....s    ......
..Q.....USER root
...2
    :
```

* USER root

---

```
01:27:11.238673 2462 MHz 11n -40dBm signal antenna 3 72.2 Mb/s MCS 7 20 MHz short GI mixed IP 192.168.200.102.50504 > 192.168.200.1.21: Flags [P.], seq 11:23, ack 55, win 115, options [nop,nop,TS val 35416878 ecr 194908], length 12: FTP: PASS pqiap
E..@.@@.@./....f.....H.....F.[.&...s.......
..k....\PASS pqiap
.5.Z
```

* PASS pqiap

---

FTP接続で`root:pqiap`のようです．

---

### インターフェイスを戻す

```
sudo iw dev mon0 del
sudo iw phy phy0 interface add wls1 type managed
```

---

## ftp接続を試してみる

```
$ nc 192.168.200.1 21
220 (vsFTPd 2.0.7)
user root
331 Please specify the password.
pass pqiap
230 Login successful.
```

---

## telnetを試してみる

```
$ nc 192.168.200.1 23
������!����(none) login: 

(none) login: root
root
Password: pqiap



BusyBox v1.01 (2013.01.03-08:27+0000) Built-in shell (ash)
Enter 'help' for a list of built-in commands.

~ # uname -a
uname -a
Linux (none) 2.6.31.AirPen_V0.1.22-g5eca71a #319 Thu Jan 3 16:27:02 CST 2013 mips unknown
```

---

ということで中に入れるようになりました :)

---

```
~ # cat /proc/cpuinfo
cat /proc/cpuinfo
system type             : Atheros AR9330 (Hornet)
processor               : 0
cpu model               : MIPS 24Kc V7.4
BogoMIPS                : 232.96
wait instruction        : yes
microsecond timers      : yes
tlb_entries             : 16
extra interrupt vector  : yes
hardware watchpoint     : yes, count: 4, address/irw mask: [0x0000, 0x0048, 0x0ff8, 0x093b]
ASEs implemented        : mips16
shadow register sets    : 1
core                    : 0
VCED exceptions         : not available
VCEI exceptions         : not available
```

---

```
~ # cat /proc/meminfo
cat /proc/meminfo
MemTotal:          29208 kB
MemFree:           19432 kB
Buffers:             120 kB
Cached:             2628 kB
SwapCached:            0 kB
Active:             2536 kB
Inactive:           1240 kB
Active(anon):       1028 kB
Inactive(anon):        0 kB
Active(file):       1508 kB
Inactive(file):     1240 kB
Unevictable:           0 kB
Mlocked:               0 kB
SwapTotal:             0 kB
SwapFree:              0 kB
Dirty:                 0 kB
Writeback:             0 kB
AnonPages:          1052 kB
Mapped:             1088 kB
  :
```
※ロットによってRAMは32MB/64MBがあるらしい

<!--
Slab:               4164 kB
SReclaimable:        464 kB
SUnreclaim:         3700 kB
PageTables:          140 kB
NFS_Unstable:          0 kB
Bounce:                0 kB
WritebackTmp:          0 kB
CommitLimit:       14604 kB
Committed_AS:       2988 kB
VmallocTotal:    1048404 kB
VmallocUsed:        1404 kB
VmallocChunk:    1045032 kB
-->

---

```
~ # lsmod
lsmod
Module                  Size  Used by    Tainted: P  
umac 576480 0 - Live 0xc02b6000
ath_dev 207072 1 umac, Live 0xc01c3000 (P)
ath_rate_atheros 20032 1 ath_dev, Live 0xc0174000 (P)
ath_hal 371936 2 umac,ath_dev, Live 0xc010b000 (P)
adf 9904 3 umac,ath_dev,ath_hal, Live 0xc0096000
asf 6816 3 umac,ath_dev,ath_hal, Live 0xc0089000 (P)
athrs_gmac 49616 0 - Live 0xc0072000
usb_storage 38400 0 - Live 0xc003e000
ehci_hcd 32928 0 - Live 0xc001d000
```

---

```
~ # mount
mount
/dev/mtdblock5 on / type squashfs (ro,relatime)
/proc on /proc type proc (rw,relatime)
devpts on /dev/pts type devpts (rw,relatime,mode=600)
none on /tmp type ramfs (rw,relatime)
/sys on /sys type sysfs (rw,relatime)
udev on /dev type ramfs (rw,relatime)
/dev/pts on /dev/pts type devpts (rw,relatime,mode=600)
```

---

```
~ # ps -ef
ps -ef
  PID  Uid     VmSize Stat Command
    1 root        192 S   init       
    2 root            SW< [kthreadd]
    3 root            SW< [ksoftirqd/0]
    4 root            SW< [events/0]
    5 root            SW< [khelper]
    8 root            SW< [async/mgr]
   83 root            SW< [kblockd/0]
   92 root            SW< [khubd]
  111 root            SW  [pdflush]
  112 root            SW  [pdflush]
  113 root            SW< [kswapd0]
  142 root            SW< [mtdblockd]
  182 root            SW< [unlzma/0]
  199 root        412 S   rc init 
  200 root        216 S   wdog 
  202 root        196 S   /sbin/getty ttyS0 115200 
  205 root            SW< [scsi_eh_0]
  206 root            SW< [usb-storage]
  253 root        376 S < /sbin/udevd -d 
  402 root        308 S   hostapd -B /tmp/secbr0 
  406 root        160 S   udhcpd /etc/udhcpd_br0.conf 
  430 root        404 S   shttpd -root /tmp/www/ftp -ports 8080 
  432 root        356 S   /sbin/vsftpd 
  438 root        132 S   /usr/sbin/telnetd 
  442 nobody      396 S   /sbin/dnsmasq 
  450 root         84 S   /usr/sbin/httpd -h /tmp/www/ 
  488 root        304 R   -sh 
```
---

# GPIOを触ってみる

* /proc/gpio/ があったので叩いてみる

```
# ls -lA /proc/gpio/
ls -lA /proc/gpio/
-r--r--r--    1 root     root            0 Jan  1 07:22 gpio12_in
-r--r--r--    1 root     root            0 Jan  1 07:22 gpio22_in
-rw-r--r--    1 root     root            0 Jan  1 07:22 gpio23_out
-rw-r--r--    1 root     root            0 Jan  1 07:22 gpio27_out
-r--r--r--    1 root     root            0 Jan  1 07:22 gpio6_in
-rw-r--r--    1 root     root            0 Jan  1 07:22 gpio7_out
-r--r--r--    1 root     root            0 Jan  1 07:22 gpio8_in
```

---

* 赤LED(🔋) : /proc/gpio/gpio7_out
	* 0 : On
	* 1 : Off

```
/proc/gpio # echo 1 > gpio7_out
echo 1 > gpio7_out
/proc/gpio # echo 0 > gpio7_out
echo 0 > gpio7_out
```

---

* 黄緑LED(🔃) : /proc/gpio/gpio23_out
	* 0 : Off
	* 1 : On

```
/proc/gpio # echo 0 > gpio23_out
echo 0 > gpio23_out
/proc/gpio # echo 1 > gpio23_out
echo 1 > gpio23_out
```

---

* 横面同期ボタン(🔃) : /proc/gpio/gpio22_in
  * 0 : On
  * 1 : Off

```
/proc/gpio # cat gpio22_in
cat gpio22_in
0
/proc/gpio # cat gpio22_in
cat gpio22_in
1
```

---

という感じで2つのLEDと1つのボタンは簡単に利用できました．他はちょっと叩いただけでは割らなかったです．
とりあえずこんな感じで横の同期ボタンを押すとLEDx2を光らせるということが出来ます．

```
~ # while :
> do
> if [ `cat /proc/gpio/gpio22_in` = '0' ]; then
> echo on
> echo 0 > /proc/gpio/gpio7_out
> echo 1 > /proc/gpio/gpio23_out
> break
> fi
> sleep 1
> done
```

---

<iframe width="560" height="315" src="https://www.youtube.com/embed/Ywan8-NJ-Dk?rel=0" frameborder="0" allowfullscreen></iframe>
https://twitter.com/matoken/status/840754952670597120

---

# microSDを用意

* microSDのない状態でAir Penにデータを書き込んでも消えてしまう
* microSDを用意してそこにデータを置くことに

---

# ext4が使えない

* 対応していないのでext2で……．

```
# cat /proc/filesystems
nodev   sysfs
nodev   rootfs
nodev   bdev
nodev   proc
nodev   sockfs
nodev   usbfs
nodev   pipefs
nodev   tmpfs
nodev   inotifyfs
nodev   devpts
		ext2
		squashfs
nodev   ramfs
		vfat
		msdos
		ntfs
nodev   autofs
nodev   fuse
		fuseblk
nodev   fusectl
```

---

# microSDのマウント場所

* `/tmp/www/ftp`以下にマウントされる
* 複数パーティションの場合はすべてマウントされる
* 以下はfatとext2の2つのパーティションを用意した時の例

```
~ # mount
mount
/dev/mtdblock5 on / type squashfs (ro,relatime)
/proc on /proc type proc (rw,relatime)
devpts on /dev/pts type devpts (rw,relatime,mode=600)
none on /tmp type ramfs (rw,relatime)
/sys on /sys type sysfs (rw,relatime)
udev on /dev type ramfs (rw,relatime)
/dev/pts on /dev/pts type devpts (rw,relatime,mode=600)
/dev/sda1 on /tmp/www/ftp/sda1 type vfat (rw,relatime,fmask=0022,dmask=0022,codepage=cp437,iocharset=utf8,errors=remount-ro)
/dev/sda2 on /tmp/www/ftp/sda2 type ext2 (rw,relatime,errors=continue)
```

---

# 使いづらいので新しいbusyboxを置いておく

```
$ wget https://www.busybox.net/downloads/binaries/1.26.2-defconfig-multiarch/busybox-mips \
-O - | curl -T - -u root:pqiap \
--ftp-create-dirs ftp://192.168.2.211/sda1/bin/
busybox
```

---

# 新しいbusyboxを優先するように

```
# cat busybox-link.sh
cat busybox-link.sh
#!/bin/sh

BUSYBOX="/tmp/www/ftp/sda1/bin/busybox"
PREFIX="/tmp/opt"

mkdir -p ${PREFIX}/bin
mkdir -p ${PREFIX}/sbin
mkdir -p ${PREFIX}/usr/bin
mkdir -p ${PREFIX}/usr/sbin
cd ${PREFIX}
${BUSYBOX} --list-full | while read cmd
do
  echo "#-- ${cmd}"
#  ${BUSYBOX} rm ${cmd}
  echo ${BUSYBOX} ln -s ${BUSYBOX} ${PREFIX}/${cmd}
  ${BUSYBOX} ln -s ${BUSYBOX} ${PREFIX}/${cmd}
done

PATH=${PREFIX}/bin:${PREFIX}/usr/bin:${PREFIX}/sbin:${PREFIX}/usr/sbin:${PATH}
```

---

# もっと色々なコマンドを使いたい

* Debianにはmipsのarchがある
* chroot環境を作ると便利かも?

---

# hostPCでchroot環境作成

debootstrapで

```
$ mkdir debian_stable_mips
$ fakeroot
# /usr//sbin/debootstrap --foreign --arch=mipsel stable debian_stable_mips
```

とかしてイメージを作成してmicroSDにcpする

---

# Air Penでchroot

* エラーorz


```
~ # /tmp/www/ftp/sda1/bin/busybox chroot /tmp/www/ftp/sda2 /bin/sh
/tmp/www/ftp/sda1/bin/busybox chroot /tmp/www/ftp/sda2 /bin/sh
FATAL: kernel too old
```

---

# oldoldstableのsqueezeを試す

* versionが近そうなsqueezeのイメージを試すといけた

```
# /tmp/www/ftp/sda1/bin/busybox chroot /tmp/www/ftp/sda2 /bin/sh
/tmp/www/ftp/sda1/bin/busybox chroot /tmp/www/ftp/sda2 /bin/sh/tmp/www/ftp/sda1/bin/busybox chroot /tmp/www/ftp/sda2 /bin/sh
```

セキュリティ??

---

# aptとかが使えるようにネットワークとaptの設定

```
$ cp /etc/hosts /tmp/www/ftp/sda2/etc/
$ cp /etc/resolv.conf /tmp/www/ftp/sda2/etc/
$ echo 'deb http://ftp.riken.jp/Linux/debian/debian-archive/debian/ squeeze main' > /etc/apt/sources.list
```
---

```
~ # mount --rbind /proc /tmp/www/ftp/sda2/proc
~ # mount --rbind /sys /tmp/www/ftp/sda2/sys 

```
---

# aptが使えるようになった

```
# apt-get update
    :
W: GPG error: http://ftp.riken.jp squeeze Release: No keyring installed in /etc/apt/trusted.gpg.d/.
# apt-get install dropbear
    :
WARNING: The following packages cannot be authenticated!
  gcc-4.4-base libc-bin libgcc1 libc6 zlib1g dropbear
Install these packages without verification [y/N]? y
    :
```

鍵が……．

---

# dropbearでsshを試す

hostのユーザ情報をcpしてchroot後dropbearを起動

```
~ # cp /etc/passwd /tmp/www/ftp/sda2/etc/
~ # cp /etc/group /tmp/www/ftp/sda2/etc/
~ # cp -p /etc/shadow /tmp/www/ftp/sda2/etc/
~ # /tmp/www/ftp/sda1/bin/busybox chroot /tmp/www/ftp/sda2 /bin/bash
root@(none):/# dropbear
```

---

# 別マシンから接続……繋がらない

* cipherの中にはあるけどマッチしないとなる

```
$ ssh root@192.168.2.202
Unable to negotiate with 192.168.2.202 port 22: no matching key exchange method found. Their offer: diffie-hellman-group1-sha1
$ ssh -Q kex | grep diffie-hellman-group1-sha1
diffie-hellman-group1-sha1
```

---

# sshオプションで解決したけどPTYがない

* diffie-hellman-group1-sha1はサポートから外されているのでオプションに指定して強制
* PTYに繋がらないのでとりあえずbash -iで入る

```
$ apt-get changelog openssh-server | grep -A2 diffie-hellman-group1-sha1
    - Support for the 1024-bit diffie-hellman-group1-sha1 key exchange is
      disabled by default at run-time.  It may be re-enabled using the
      instructions at http://www.openssh.com/legacy.html
$ ssh -o KexAlgorithms=+diffie-hellman-group1-sha1 root@192.168.2.202
PTY allocation request failed
$ ssh -o KexAlgorithms=+diffie-hellman-group1-sha1 root@192.168.2.202 /bin/bash -i
```

---

# PTYを用意する

* Air Penのchroot前にmountしておく
--rbindを使いたいけど非対応

```
~ # mount -o bind /dev ${DEBIANROOT}/dev
~ # mount -t proc none ${DEBIANROOT}/proc
~ # mount -o bind /sys ${DEBIANROOT}/sys
~ # mount -t devpts none ${DEBIANROOT}/dev/pts
```

---

# chroot scriptにまとめておく

```
#!/bin/sh

export DEBIANROOT=/tmp/www/ftp/sda2
cp /etc/hosts ${DEBIANROOT}/etc/
cp /etc/resolv.conf ${DEBIANROOT}/etc/
cp /etc/passwd ${DEBIANROOT}/etc/
cp /etc/group ${DEBIANROOT}/etc/
cp /etc/shadow ${DEBIANROOT}/etc/

echo > ${DEBIANROOT}/etc/mtab

mount -o bind /dev ${DEBIANROOT}/dev
mount -t proc none ${DEBIANROOT}/proc
mount -o bind /sys ${DEBIANROOT}/sys
mount -t devpts none ${DEBIANROOT}/dev/pts

/tmp/www/ftp/sda1/bin/busybox chroot ${DEBIANROOT} /bin/bash
```

---

# とりあえず500円分以上には遊んだ感

---

# source code請求

* PQIの問い合わせメールアドレスにGPLなソース欲しいけど請求方法教えてと投げる
* 1週間ほどでダウンロードリンクがメールで届く

* 俺たちの戦いはこれからだ!?

---

# 参考文献

* [PQI Air Pen - PQIグループ- モバイル向け周辺機器の総合メーカー　 ［Apple アクセサリ, モバイルアクセサリ, モバイルバッテリー, USB フラッシュドライブ, Wi-Fi ストレージ, 充電器］](http://jp.pqigroup.com/prod_in.aspx?prodid=539&modid=145 "PQI Air Pen - PQIグループ- モバイル向け周辺機器の総合メーカー　 ［Apple アクセサリ, モバイルアクセサリ, モバイルバッテリー, USB フラッシュドライブ, Wi-Fi ストレージ, 充電器］")
* [Debian -- ディストリビューションアーカイブ](https://www.debian.org/distrib/archive "Debian -- ディストリビューションアーカイブ")
* [chroot - Debian Wiki](https://wiki.debian.org/chroot "chroot - Debian Wiki")

