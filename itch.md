<!-- $theme: default -->

<!-- page_number: true -->
<!-- $size: 4:3 -->
<!-- footer: @matoken -->


# ゲーミングプラットホームのitch

##### KenichiroMATOHARA([@matoken](https://twitter.com/matoken/))
##### http://matoken.org 
### <s>[鹿児島Linux勉強会2017.04(2017-04-08)](https://kagolug.connpass.com/event/52477/)</s>
### <s>サンエールかごしま 小研修室3</s>
### 時間切れ未発表

---

# [KenichiroMATOHARA](http://matoken.org)( [@matoken](https://twitter.com/matoken) )
  * 大隅から来ました
  * PC-UNIX/OSS, OpenStreetMap, 電子工作, 自転車……
  * altanativeが好き
    多様性は正義

![](./osmo.jpg)

---

# [![](itchio-450.png)](https://itch.io/)[https://itch.io/](https://itch.io/)

* マルチプラットホームなゲーミングプラットホーム
  `Games for Linux (8,666 results)`
* Steam等に比べてローポリなゲームが多い感じ
* PICO-8と同じものがあったりも
* GAME JAMが頻繁に開催されているようで結構活発な感じ
<!-- 興味のあるJAMの中から好みのゲームを探索したりするもの良さそう -->
* Linuxはi386/amd64だけなのでPICO-8やSteamみたいにRaspberry Piとかで遊ぶとかは基本無理
* firejailを利用したsandboxも利用可能

---

# ゲーム以外のコンテンツも

![](itch-contents.jpg)![](itch-book.jpg)

---

# 導入

* 各種環境向けの導入手順
[Installing on Linux · The itch app manual - itch.io](https://itch.io/docs/itch/installing/linux/ "Installing on Linux · The itch app manual - itch.io")
* 今回はDebian

```
$ curl https://dl.itch.ovh/archive.key | sudo apt-key add -
$ echo 'deb https://dl.bintray.com/itchio/deb jessie main' | sudo tee /etc/apt/sources.list.d/itchio.list
deb https://dl.bintray.com/itchio/deb jessie main
$ sudo apt update && sudo apt upgrade && sudo apt install itch
```

---

# 遊んだゲームをいくつか

---

# [Raft by Redbeet Interactive](https://raft.itch.io/raft "Raft by Redbeet Interactive")

* 海で漂流している
* ガギを投げて海の漂流物を引き上げて生き延びる
* 板を幹合わせて筏を拡大したり，料理が出来るようにしたり，植物を育てたり
* そのうち海の上に筏の島のようなものが出来る
* Intel Core i5-2540M/i915では重くて辛い

---

# [Horde by yzwijsen](https://yzwijsen.itch.io/horde "Horde by yzwijsen")

* エリア内に出現する怪物を銃と爆弾で倒す
* イージー気味だけど回復アイテムなどが無いようなのでさくっと遊べる
* Intel Core i5-2540M/i915でもさくさく

---

# [Duck Build by Liam de Valmency](https://liamdev.itch.io/duck-build "Duck Build by Liam de Valmency")

* 海岸に落ちているものを組み合わせて筏を作りあひるが溺れないように頑張る（あひるなのに沈む）
* 4人までの対戦が可能
* あひる焼き的なゲーム?
* Intel Core i5-2540M/i915でもさくさく


※ あひるをモチーフにしたゲームは沢山あるのでいろいろ試したい