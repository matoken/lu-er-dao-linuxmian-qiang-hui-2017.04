# [鹿児島Linux勉強会 2017.04](https://kagolug.connpass.com/event/52477/)

日時 : 04/08 14:00-17:00(準備撤収時間込み)
会場 : サンエールかごしま 小研修室3
住所 : 鹿児島市荒田一丁目4番1号
http://www.qtopianet.com/introduction/annai.htm

## ext4の暗号化FS機能を試してみる

* [ext4encriptingfilesystem.md](ext4encriptingfilesystem.md) ( [ext4encriptingfilesystem.pdf](ext4encriptingfilesystem.pdf) )
    * [偏執的な人の為の? NotePCセキュリティ HDD編 -小江戸らぐ1月のオフな集まり(第102回](102.pdf) ※参考資料

## PQI Air Pen Hack

* [PQIAirPen.md](PQIAirPen.md) ( [PQIAirPen.pdf](PQIAirPen.pdf) )

## ゲーミングプラットホームのitch

* [itch.md](itch.md) ( [itch.pdf](itch.pdf) )

